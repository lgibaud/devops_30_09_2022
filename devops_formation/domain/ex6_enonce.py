#PURPOSE: refactor the code such that domain concepts emerge. This will simplify your code, and make it more reusable

import numpy as np

def evaluate_price(surface: float, n_room: int, floor: int, street_number: int, road_name: str, postal_code: str, city: str):
    """Returns the estimated price of an habitation"""
    RANDOM_VALUE = 2e5
    return RANDOM_VALUE


def investment_score(surface: float, n_room: int, floor: int, price: float, agency_fees: float, street_number: int, road_name: str, postal_code: str, city: str):
    """Returns the difference between the price and the estimated price. More negative values are better"""
    estimated_price = evaluate_price(surface, n_room, floor, street_number, road_name, postal_code, city)
    total_price = price + agency_fees
    score = estimated_price - total_price
    return score